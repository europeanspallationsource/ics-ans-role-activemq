ics-ans-role-activemq
=====================

Ansible role to install Apache ActiveMQ.

Web consoles, REST and Ajax APIs are disabled.
Only TCP transport (port 616616) is enabled.

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

```yaml
activemq_version: 5.15.2
activemq_url: https://artifactory.esss.lu.se/artifactory/swi-pkg/apache/activemq/{{ activemq_version }}/apache-activemq-{{ activemq_version }}-bin.tar.gz

# ActiveMQ users
# They will all be added to the admins group
activemq_users:
  - name: admin
    password: admin
  - name: user
    password: user
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-activemq
```

License
-------

BSD 2-clause
